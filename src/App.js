import React from 'react';
import classnames from 'classnames';

import Form from 'components/Form';

import './scss/main.scss';
import styles from './App.module.scss';

import { ReactComponent as Pointer } from 'static/icons/pointer.svg';

// import fonts
require('typeface-varta')
require('typeface-open-sans')

const App = ()=> {
  return (
    <div className={styles.app}>
      <section>
        <div className="container">

          <header>
            <h2 className={styles.heading}>Essentials</h2>
          </header>

          <div className={styles.columns}>
            <div className={styles.column}>
              <strong className={styles.strong}>Great value health cover to fit your lifestyle from less than <span>$4.40</span> a week^</strong>
              <p>
                A healthy body can do amazing things! Look after yours with Essentials Saver. You’re instantly covered for Urgent Ambulance and General Dental, and you get to choose two more services you will use, so you don’t pay for what you won’t.
              </p>
              <ul className={styles.bulletlist}>
                <li><span className={classnames(styles.bullet, styles.four)}>4</span>Four essential services to fit your lifestyle</li>
                <li><span className={classnames(styles.bullet, styles.pointer)}><Pointer /></span>Choose the 2 services that you will use and get 2 more</li>
              </ul>
            </div>

            <div className={styles.column}>
              <Form />
            </div>
            
          </div>
        </div>
      </section>
    </div>
  );
}

export default App;
import React from 'react';

import styles from './Checkbox.module.scss';

const Checkbox = ({
  handleInputChange,
  id,
  label,
  checked,
  disabled
}) => {
  return (
    <div className={styles.fieldgroup}>
      <input id={id} onClick={handleInputChange} defaultChecked={checked} disabled={disabled} name={id} type="checkbox" aria-checked="false" aria-labelledby={`${id}-label`} />
      <label id={`${id}-label`} htmlFor={id}>
        {label}
      </label>
  </div>
  )
}

export default Checkbox
import React, { useState, useEffect } from 'react';
import classnames from 'classnames';

import Checkbox from './Checkbox';
import { ReactComponent as Chevron } from 'static/icons/chevron.svg';

import styles from './Form.module.scss';


const Form = () => {
  // set default states
  const [ values, setValues ] = useState({})  
  const [ checked, setChecked ] = useState(2);
  const [ disabled, setDisabled ] = useState(true);
  const [ interacted, setInteracted ] = useState(false);

  const onSubmit = (e)=> {
    e.preventDefault();
  }

  const handleInputChange = (e) => {
    // save checked items into values
    const { name, checked } = e.target;
    setValues({...values, [name]: checked})

    // check for initial interaction with form
    setInteracted(true);
  }

  useEffect(()=> {
    // check localStorage for existing data
    if(!interacted) {
      let retrievedObject = localStorage.getItem('services');
      let parsedData = JSON.parse(retrievedObject);
      if(retrievedObject) {
        setValues(parsedData)
      }
    }
  }, [interacted])


  useEffect(() => {
    // count checked items
    setChecked(Object.values(values).reduce((a, value) => a + value, 0))

    // set and update localStorage
    if(values) {
      localStorage.setItem('services', JSON.stringify(values))
    }    
  },[values])

  useEffect(()=> {
    // show error conditional
    checked >=2 && checked <= 5
      ? setDisabled(false)
      : setDisabled(true)
  },[checked])
  
  return (
    <>
      <h3 className={styles.heading}>Customise your cover</h3>
      <form className={styles.form} onSubmit={onSubmit}>
        <fieldset className={styles.fieldset}>
          <legend className={styles.legend}>Your cover includes:</legend>
          <Checkbox id="urgentambulance" label="Urgent Ambulance" checked disabled/>
          <Checkbox id="general-dental" label="General Dental" checked disabled/>
        </fieldset>

        <fieldset className={styles.fieldset}>
          <legend className={styles.legend}>
            You choose 2 more services:
            <span className={classnames(styles.error, {[styles.active] : interacted})}>
              {
                // show message based on x checked markers
                checked <= 1
                  ? 'Please select 2 or more services'
                  : checked >= 6
                    ? 'Please select 5 or fewer services'
                    : null
              }
            </span>
          </legend>
          <Checkbox handleInputChange={handleInputChange} id="generaldental" checked={values.generaldental} label="General Dental" />
          <Checkbox handleInputChange={handleInputChange} id="optical" checked={values.optical} label="Optical" />
          <Checkbox handleInputChange={handleInputChange} id="physio" checked={values.physio} label="Physio" />
          <Checkbox handleInputChange={handleInputChange} id="podiatry" checked={values.podiatry} label="Podiatry" />
          <Checkbox handleInputChange={handleInputChange} id="pharmacy" checked={values.pharmacy} label="Pharmacy" />
          <Checkbox handleInputChange={handleInputChange} id="chiro" checked={values.chiro} label="Chiro/Osteo" />
          <Checkbox handleInputChange={handleInputChange} id="remedialmassage" checked={values.remedialmassage} label="Remedial Massage" />
        </fieldset>   
        <button className={styles.submit} disabled={disabled}>Get a quote <Chevron /></button>
      </form>
    </>
  )
}

export default Form;